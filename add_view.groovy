#!groovy

/*
 * Adds view to jenkins
 */

import jenkins.model.*
import hudson.model.ListView

if ( ! jenkins_instance.getView("kduchno_view") )
{
  def newView = new hudson.model.ListView("kduchno_view")
  jenkins_instance.addView(newView)
}

def view = jenkins_instance.getView("kduchno_view")
view.getColumns().remove(hudson.views.WeatherColumn)

// eof
