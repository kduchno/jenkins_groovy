#!groovy

/*
 * Script that add ssh nodes to Jenkins
 */

import hudson.model.*
import jenkins.model.*
import hudson.slaves.*
import hudson.plugins.sshslaves.SSHLauncher
import hudson.plugins.sshslaves.verifiers.*
import com.cloudbees.plugins.credentials.impl.*;
import com.cloudbees.plugins.credentials.*;
import com.cloudbees.plugins.credentials.domains.*;

// Pick one of the strategies from the comments below this line
// SshHostKeyVerificationStrategy hostKeyVerificationStrategy = new KnownHostsFileKeyVerificationStrategy()
    //= new KnownHostsFileKeyVerificationStrategy() // Known hosts file Verification Strategy
    //= new ManuallyProvidedKeyVerificationStrategy("<your-key-here>") // Manually provided key Verification Strategy
    //= new ManuallyTrustedKeyVerificationStrategy(false /*requires initial manual trust*/) // Manually trusted key Verification Strategy
    //= new NonVerifyingKeyVerificationStrategy() // Non verifying Verification Strategy

SshHostKeyVerificationStrategy noVerification = new NonVerifyingKeyVerificationStrategy()
SshHostKeyVerificationStrategy knowHostsVerification = new KnownHostsFileKeyVerificationStrategy()

String getCredentials(String username = "jenkins") {
    def creds = com.cloudbees.plugins.credentials.CredentialsProvider.lookupCredentials(
        com.cloudbees.plugins.credentials.common.StandardUsernameCredentials.class,
        jenkins.model.Jenkins.instance
    )

    def c = creds.findResult { it.username == username ? it : null }

    return c.id.toString()
}

ComputerLauncher setupLauncher(
    String ip,
    int ssh_port,
    SshHostKeyVerificationStrategy hostKeyVerificationStrategy) {
    ComputerLauncher launcher = new SSHLauncher(
        ip, // Host
        ssh_port, // Port
        getCredentials(), // Credentials
        (String)null, // JVM Options
        (String)null, // JavaPath
        (String)null, // Prefix Start Slave Command
        (String)null, // Suffix Start Slave Command
        5, // Connection Timeout in Seconds
        0, // Maximum Number of Retries
        0, // The number of seconds to wait between retries
        hostKeyVerificationStrategy
    )
    return launcher
}

Slave setupSlave(
    String name,
    String mountpoint,
    ComputerLauncher launcher,
    Integer numExecutors) {
    // Define a "Permanent Agent"
    Slave agent = new DumbSlave(
        name,
        mountpoint,
        launcher,
    )

    agent.setLabelString("slave-node")
    agent.setNodeDescription("Simple slave")
    agent.setNumExecutors(numExecutors)

    return agent
}

// Example(S)
Jenkins.instance.addNode(setupSlave("slave-1", "/home/jenkins/slaveshare", setupLauncher("test_slave_1", 22, noVerification), 5))
Jenkins.instance.addNode(setupSlave("slave-2", "/home/jenkins/slaveshare", setupLauncher("test_slave_2", 22, knowHostsVerification), 10))

// eof
