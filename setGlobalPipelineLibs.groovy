#!groovy

/*
 * This script add global libs to pipeline using modernSCM
 * https://bitbucket.org/kduchno/test_lib.git -> change for desired repo
 * "test_lib" -> change for desired name
 * "master" -> change for desired version
 */

import jenkins.model.*
import org.jenkinsci.plugins.workflow.libs.SCMSourceRetriever;
import org.jenkinsci.plugins.workflow.libs.LibraryConfiguration;
import jenkins.plugins.git.GitSCMSource;
import jenkins.scm.api.trait.SCMSourceTrait;
import jenkins.plugins.git.traits.*

def inst = jenkins.model.Jenkins.instance
def descr = inst.getDescriptor("org.jenkinsci.plugins.workflow.libs.GlobalLibraries")
List<SCMSourceTrait> traits = new ArrayList<>();

/* here should go url to lib */
/* test_lib has step test(<username>) which extracts credentialsID for username
 * or null if nout found, but DOES NOT VERIFY - just simple example
 */
GitSCMSource test_lib = new GitSCMSource("https://bitbucket.org/kduchno/test_lib.git")

SCMSourceRetriever retriever = new SCMSourceRetriever(test_lib)

/* name of new lib */
LibraryConfiguration pipeline = new LibraryConfiguration("test_lib", retriever)

/* set default version or branch */
pipeline.setDefaultVersion("master")

/* adding traits to config -> http://javadoc.jenkins.io/plugin/git/jenkins/plugins/git/traits/package-summary.html */
def trait = new BranchDiscoveryTrait()
traits.add(trait)
test_lib.setTraits(traits)

descr.setLibraries([pipeline])

// eof
