#!groovy

/*
 * Script that lists all descriptors and their getMethods
 *
 * One of descriptors is responsible for SSH port and using its
 * method, that port can be manipulated
 */

import javaposse.jobdsl.plugin.GlobalJobDslSecurityConfiguration
import jenkins.model.GlobalConfiguration

println("--------------------------------------------")

globalConfig = GlobalConfiguration.all().any {
  println("Getting methods for descriptor ".toUpperCase() + it.toString() + "\n")
  it.class.getMethods().any { method ->
    println(method)
  }
  println("")
}

println("--------------------------------------------")

// eof
