#!groovy

/*
 * scripts that can create job as well as update its xml
 */

import jenkins.model.*
import org.jenkinsci.plugins.workflow.job.WorkflowJob
import javax.xml.transform.stream.StreamSource

inst = jenkins.model.Jenkins.instance

def create_job(String jobname, String job_type) {
    def project = null

    switch(job_type) {
        case "pipeline":
            project = WorkflowJob;
            break;
        case "fs":
            project = FreeStyleProject;
            break;
        default:
            project = FreeStyleProject
            break;
    }

    // create pipeline project
    inst.createProject(project, jobname)
}

def update_jobs_config(String jobname, String filepath) {
    // find job
    job = inst.getItem(jobname)

    // creating StreamSource for updateByXml method
    ss = new StreamSource(filepath)

    // update jobs xml from some origin
    job.updateByXml(ss)
}

// examples
create_job("delete_single_job", "fs")
create_job("delete_jobs_pipeline", "pipeline")

// providing files are in $JENKINS_HOME/jobs/
path1 = inst.root.toString().concat("/jobs/delete_single_job.xml")
path2 = inst.root.toString().concat("/jobs/delete_jobs_pipeline.xml")

update_jobs_config("delete_single_job", path1)
update_jobs_config("delete_jobs_pipeline", path2)

// eof
