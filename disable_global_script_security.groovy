#!groovy

/*
 * Script that disables global dsl script security
 */

import jenkins.model.*
import javaposse.jobdsl.plugin.GlobalJobDslSecurityConfiguration
import jenkins.model.GlobalConfiguration

println("--------------------------------------------")

println "--> disabling scripts security for job dsl scripts"

//way 1
inst = jenkins.model.Jenkins.instance
descr = inst.getDescriptor("javaposse.jobdsl.plugin.GlobalJobDslSecurityConfiguration")

descr.setUseScriptSecurity(false)

println("--------------------------------------------")

//way 2
//GlobalConfiguration.all().get(GlobalJobDslSecurityConfiguration.class).useScriptSecurity=false

// eof
