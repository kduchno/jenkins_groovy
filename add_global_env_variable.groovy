#!groovy

/*
 * Script to add global environemtal variable
 */

import jenkins.model.*
import hudson.slaves.EnvironmentVariablesNodeProperty

instance = jenkins.model.Jenkins.instance
globalNodeProperties = instance.getGlobalNodeProperties()
envVarsNodePropertyList = globalNodeProperties.getAll(EnvironmentVariablesNodeProperty.class)
newEnvVarsNodeProperty = null
envVars = null

if ( envVarsNodePropertyList == null || envVarsNodePropertyList.size() == 0 ) {
    newEnvVarsNodeProperty = new EnvironmentVariablesNodeProperty()
    globalNodeProperties.add(newEnvVarsNodeProperty)
    envVars = newEnvVarsNodeProperty.getEnvVars()
} else {
    envVars = envVarsNodePropertyList.get(0).getEnvVars()
}

// example
envVars.put("ARTIFACTORY_URL", "http://127.0.0.1:8090/artifactory")

instance.save()

// eof
