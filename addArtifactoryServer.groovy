#!groovy

/*
 * This script sets up artifactory server and credentials
 * for both deploying and resolving artifacts
 */

import jenkins.model.*
import org.jfrog.*
import org.jfrog.hudson.*
import org.jfrog.hudson.util.Credentials;

inst = jenkins.model.Jenkins.instance

def desc = inst.getDescriptor("org.jfrog.hudson.ArtifactoryBuilder")

// use default jfrog 'out of the box' settings
def deployerCredentials = new Credentials("admin", "password")
def resolverCredentials = new Credentials("", "")

def deployerCredentialsConfig = new CredentialsConfig(
  deployerCredentials,
  java.util.UUID.randomUUID().toString(),
  false
)

def resolverCredentialsConfig = new CredentialsConfig(
  resolverCredentials,
  java.util.UUID.randomUUID().toString(),
  false
)

def new_servs = [
  new ArtifactoryServer(
    "<name>",
    "http://<name & port if running in one network in docker or IP:port>/artifactory",
    deployerCredentialsConfig,
    resolverCredentialsConfig,
    300,
    false,
    3
  )
]

desc.setArtifactoryServers(new_servs)

// eof
