#!groovy

/*
 * Script that adds credentials based on ssh keys in
 * masters .ssh dir to GLOBAL scope
 */

import com.cloudbees.plugins.credentials.*
import com.cloudbees.plugins.credentials.common.*
import com.cloudbees.plugins.credentials.domains.*
import com.cloudbees.jenkins.plugins.sshcredentials.impl.*

def createGlobalMasterSSHCreds(String username = "jenkins") {
    Credentials c = (Credentials) new BasicSSHUserPrivateKey(
        CredentialsScope.GLOBAL,
        java.util.UUID.randomUUID().toString(),
        username,
        new BasicSSHUserPrivateKey.UsersPrivateKeySource(),
        "", // passphrase
        "description"
    )

    SystemCredentialsProvider.getInstance().getStore().addCredentials(Domain.global(), c)
}

createGlobalMasterSSHCreds("jenkins")

// eof
