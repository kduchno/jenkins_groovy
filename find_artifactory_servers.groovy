#!groovy

/*
 * Simple script to list configured artifactory servers
 */

import jenkins.model.*
import org.jfrog.*
import org.jfrog.hudson.*

// get artifactory url
rt_descriptor = jenkins.model.Jenkins.instance.getDescriptor("org.jfrog.hudson.ArtifactoryBuilder")
rt_servers = rt_descriptor.getArtifactoryServers()

rt_servers.find{ server ->
    println(server, server.getUrl())
}

// eof
