#!groovy

/*
 * Scripts that setups basic security for jenkins server
 */

import jenkins.model.*
import hudson.security.*
import hudson.security.csrf.DefaultCrumbIssuer
import jenkins.security.s2m.AdminWhitelistRule

// get jenkins instance
def jenkins_instance = Jenkins.instance

// set authorization strategy and create user
def hudsonRealm = new HudsonPrivateSecurityRealm(false)
hudsonRealm.createAccount("jenkins", "jenkins")
jenkins_instance.setSecurityRealm(hudsonRealm)

def strategy = new FullControlOnceLoggedInAuthorizationStrategy()
jenkins_instance.setAuthorizationStrategy(strategy)

// Disable CLI over remoting
jenkins_instance.getInjector().getInstance(AdminWhitelistRule.class).setMasterKillSwitch(false)

// disable CLI over remoting
jenkins_instance.getDescriptor("jenkins.CLI").get().setEnabled(false)

// remove deprecated protocols
HashSet<String> newProtocols = new HashSet<>(jenkins_instance.getAgentProtocols());
newProtocols.removeAll(Arrays.asList(
        "JNLP3-connect", "JNLP2-connect", "JNLP-connect", "CLI-connect", "CLI2-connect"
));
jenkins_instance.setAgentProtocols(newProtocols);

// set ssh port
jenkins_instance.getDescriptor("org.jenkinsci.main.modules.sshd.SSHD").setPort(22)

// enable CSRF protection
jenkins_instance.setCrumbIssuer(new DefaultCrumbIssuer(true))

// save settings for jenkins
jenkins_instance.save()

// eof
