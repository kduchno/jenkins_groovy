#!groovy

/*
 * Change admin mail address
 */

import jenkins.model.*

def jenkins_instance = Jenkins.instance

String admin_mail = "username <email address>"

def jenkinsLocationConfiguration = JenkinsLocationConfiguration.get()
jenkinsLocationConfiguration.setAdminAddress(admin_mail)

jenkinsLocationConfiguration.save()

def check_admin_mail = jenkinsLocationConfiguration.getAdminAddress()

// If this asserts fail, password setting for admin doesn't work
assert check_admin_mail == admin_mail

jenkins_instance.save()

// eof
