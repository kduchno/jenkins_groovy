#!groovy

/*
 * Setting smtp for mails
 */

import jenkins.model.*
import hudson.util.Secret

def inst = Jenkins.getInstance()

def mail = inst.getDescriptor("hudson.tasks.Mailer")
def ext_mail = inst.getDescriptor("hudson.plugins.emailext.ExtendedEmailPublisher")

// Thread.start{
    // sleep 10000
    println "--> setting smtp"

    mail.setSmtpAuth("<smtp user>", "<smtp password for user>")
    mail.setSmtpHost("<smtp host example> smtp.gmail.com")
    mail.setUseSsl(true)
    mail.setSmtpPort("<smtp port>465")
    mail.setCharset("UTF-8")
    mail.setReplyToAddress("<email address>")

    // extended email
    ext_mail.smtpAuthUsername = "<email address>"
    ext_mail.smtpAuthPassword = Secret.fromString("pgcotkfxxkhuvqcz")
    ext_mail.smtpHost = "smtp.gmail.com"
    ext_mail.smtpPort = 465
    ext_mail.charset = "UTF-8"
    ext_mail.useSsl = "true"
    ext_mail.defaultReplyTo = "<email address>"
    ext_mail.defaultContentType = "text/html"
    ext_mail.defaultSubject="\$PROJECT_NAME - Build # \$BUILD_NUMBER - \$BUILD_STATUS!"
    ext_mail.defaultBody="\$PROJECT_NAME - Build # \$BUILD_NUMBER - \$BUILD_STATUS:\n\nCheck console output at \$BUILD_URL to view the results."

    inst.save()
// }
// eof
