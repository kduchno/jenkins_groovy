#!groovy

/*
 * Set git config
 */

import jenkins.model.*

def jenkins_instance = Jenkins.instance

def git_descriptor = jenkins_instance.getDescriptor("hudson.plugins.git.GitSCM")

git_descriptor.setGlobalConfigName("<user for git>")
git_descriptor.setGlobalConfigEmail("<email for git>")

jenkins_instance.save()

// eof
