#!groovy

/*
 * Script that adds secret text credentials to GLOBAL scope
 */

import com.cloudbees.plugins.credentials.*
import com.cloudbees.plugins.credentials.common.*
import com.cloudbees.plugins.credentials.domains.*
import org.jenkinsci.plugins.plaincredentials.impl.StringCredentialsImpl

def createGlobalSecretText(String text_to_secret) {
    Credentials c = (Credentials) new StringCredentialsImpl(
        CredentialsScope.GLOBAL,
        java.util.UUID.randomUUID().toString(),
        "description",
        new hudson.util.Secret(text_to_secret)
        )

    SystemCredentialsProvider.getInstance().getStore().addCredentials(Domain.global(), c)
}

createGlobalSecretText("kopytko")

// eof
