#!groovy

/*
 * Script that adds USER:PASSWORD credentials to GLOBAL scope
 */

import com.cloudbees.plugins.credentials.impl.*;
import com.cloudbees.plugins.credentials.*;
import com.cloudbees.plugins.credentials.domains.*;

def bitbucket_user = "<bitbucket user/login>"
def bitbucket_app_pass = "<bitbucket password or app password>"

def createUserPassword(String username, String password) {
    Credentials c = (Credentials) new UsernamePasswordCredentialsImpl(
        CredentialsScope.GLOBAL,
        java.util.UUID.randomUUID().toString(),
        'description',
        username,
        password
    )

    SystemCredentialsProvider.getInstance().getStore().addCredentials(Domain.global(), c)
}

createUserPassword(bitbucket_user, bitbucket_app_pass)

// eof
