#!groovy

/*
 * Script that setup global gitlab connection
 * also clickable in global config
 */

import jenkins.model.*
import com.dabsquared.gitlabjenkins.connection.*
import com.dabsquared.gitlabjenkins.gitlab.api.impl.*

inst = jenkins.model.Jenkins.instance
desc = inst.getDescriptor("com.dabsquared.gitlabjenkins.connection.GitLabConnectionConfig")

def createGitlabConnection(
    String connection_name, String connection_url,
    String credentialsID, String api_version = "v4") {

    apiBuilder = (api_version == "v3") ? new V3GitLabClientBuilder() : new V4GitLabClientBuilder()

    GitLabConnection newConnection = new GitLabConnection(
        connection_name,
        connection_url,
        credentialsID,
        apiBuilder,
        true,
        10,
        10
        )

    desc.addConnection(newConnection)
}

createGitlabConnection(
    "<connection_name>", "<connection_url>",
    "<credentialsID>", "<api_version>"
)

// eof
