#!groovy

/*
 * Script that changes security to matrix-based with some mixture of
 * permissions for user
 * authenticated - for all logable users
 * anonymous - for not logged
 */

import hudson.model.*
import hudson.security.*
import hudson.PluginManager

inst = jenkins.model.Jenkins.instance
realm = new HudsonPrivateSecurityRealm(false, false, null)
strategy = new GlobalMatrixAuthorizationStrategy()

slave_permissions = [
    hudson.model.Computer.BUILD,
    hudson.model.Computer.CONFIGURE,
    hudson.model.Computer.CONNECT,
    hudson.model.Computer.CREATE,
    hudson.model.Computer.DELETE,
    hudson.model.Computer.DISCONNECT,
    hudson.model.Computer.EXTENDED_READ
]

credentials_permissions = [
    com.cloudbees.plugins.credentials.CredentialsProvider.CREATE,
    com.cloudbees.plugins.credentials.CredentialsProvider.DELETE,
    com.cloudbees.plugins.credentials.CredentialsProvider.MANAGE_DOMAINS,
    com.cloudbees.plugins.credentials.CredentialsProvider.UPDATE,
    com.cloudbees.plugins.credentials.CredentialsProvider.VIEW
]

overall_permissions = [
    hudson.model.Hudson.ADMINISTER,
    hudson.model.Hudson.READ,
    hudson.model.Hudson.RUN_SCRIPTS,
    hudson.PluginManager.CONFIGURE_UPDATECENTER,
    hudson.PluginManager.UPLOAD_PLUGINS
]

job_permissions = [
    hudson.model.Item.BUILD,
    hudson.model.Item.CANCEL,
    hudson.model.Item.CONFIGURE,
    hudson.model.Item.CREATE,
    hudson.model.Item.DELETE,
    hudson.model.Item.DISCOVER,
    hudson.model.Item.EXTENDED_READ,
    hudson.model.Item.READ,
    hudson.model.Item.WIPEOUT,
    hudson.model.Item.WORKSPACE
]

run_permissions = [
    hudson.model.Run.DELETE,
    hudson.model.Run.UPDATE,
    hudson.model.Run.ARTIFACTS
]

view_permissions = [
    hudson.model.View.CONFIGURE,
    hudson.model.View.CREATE,
    hudson.model.View.DELETE,
    hudson.model.View.READ
]

scm_permissions= [
    hudson.scm.SCM.TAG
]

artifactory_permissions = [
    org.jfrog.hudson.ArtifactoryPlugin.PROMOTE,
    org.jfrog.hudson.ArtifactoryPlugin.PUSH_TO_BINTRAY,
    org.jfrog.hudson.ArtifactoryPlugin.RELEASE
]

rebuild_permissions = [
    com.sonyericsson.jenkins.plugins.bfa.PluginImpl.REMOVE_PERMISSION,
    com.sonyericsson.jenkins.plugins.bfa.PluginImpl.UPDATE_PERMISSION,
    com.sonyericsson.jenkins.plugins.bfa.PluginImpl.VIEW_PERMISSION
]

/* some permissions must be created from String, otherwise Jenkins throws errors */
others = [
    Permission.fromId("hudson.model.Run.Replay"),
    Permission.fromId("hudson.model.Item.Move")
]

minimal_package = [
    hudson.model.Item.BUILD,
    hudson.model.Item.CANCEL,
    hudson.model.Item.READ,
    hudson.model.View.READ,
    hudson.model.Hudson.READ,
]

def setStrategyForUser(ArrayList permissions, String username="authenticated") {
    permissions.any { perm ->
        strategy.add(perm, username)
    }
}

def createUser(String username, String password) {
    realm.createAccount(username, password)
}

def setAllForUser(username="authenticated") {
    setStrategyForUser(slave_permissions, username)
    setStrategyForUser(credentials_permissions, username)
    setStrategyForUser(overall_permissions, username)
    setStrategyForUser(job_permissions, username)
    setStrategyForUser(run_permissions, username)
    setStrategyForUser(view_permissions, username)
    setStrategyForUser(scm_permissions, username)
    setStrategyForUser(artifactory_permissions, username)
    setStrategyForUser(rebuild_permissions, username)
    setStrategyForUser(others, username)
}

// Example(s)
setAllForUser("jenkins")
createUser("kduchno", "kduchno")
setStrategyForUser(minimal_package, "kduchno")

inst.setAuthorizationStrategy(strategy)
inst.save()

// eof
