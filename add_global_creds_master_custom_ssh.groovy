#!groovy

/*
 * Script that adds credentials based on ssh keys in
 * PROVIDED .ssh dir to GLOBAL scope
 */

import com.cloudbees.plugins.credentials.*
import com.cloudbees.plugins.credentials.common.*
import com.cloudbees.plugins.credentials.domains.*
import com.cloudbees.jenkins.plugins.sshcredentials.impl.*

def createGlobalMasterSSHCreds(String username = "jenkins",
                               String key_path = "~/.ssh/id_rsa") {
    Credentials c = (Credentials) new BasicSSHUserPrivateKey(
        CredentialsScope.GLOBAL,
        java.util.UUID.randomUUID().toString(),
        username,
        new BasicSSHUserPrivateKey.FileOnMasterPrivateKeySource(key_path),
        "", // passphrase
        "Credentials for connecting slaves"
    )

    SystemCredentialsProvider.getInstance().getStore().addCredentials(Domain.global(), c)
}

createGlobalMasterSSHCreds("<proper_user>", "<path_to_ssh_private_key>")

// eof
