#!groovy

/*
 * simple script to check what plugins have updates
 */

inst = jenkins.model.Jenkins.instance
plugin_manager = inst.pluginManager

plugin_manager.doCheckUpdatesServer()

plugins = plugin_manager.plugins

plugins.each { plugin ->
  if(plugin.hasUpdate()) {
    println(plugin)
  }
}

//eof
