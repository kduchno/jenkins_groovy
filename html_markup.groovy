#!groovy

/*
 * Change html for "raw" to "safe html" so descriptions in jobs
 * can have html syntax
 */

import jenkins.model.*
import hudson.markup.RawHtmlMarkupFormatter

def jenkins_instance = Jenkins.instance

jenkins_instance.setMarkupFormatter(new RawHtmlMarkupFormatter(false))

jenkins_instance.save()

// eof
