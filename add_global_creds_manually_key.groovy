#!groovy

import com.cloudbees.plugins.credentials.*
import com.cloudbees.plugins.credentials.common.*
import com.cloudbees.plugins.credentials.domains.*
import com.cloudbees.jenkins.plugins.sshcredentials.impl.*

String readFileContent(String path) {
    file = new File(path)

    return file.text
}

def createGlobalMasterSSHCreds(String username, String private_key_path, String description) {
    Credentials c = (Credentials) new BasicSSHUserPrivateKey(
        CredentialsScope.GLOBAL,
        java.util.UUID.randomUUID().toString(),
        username,
        new BasicSSHUserPrivateKey.DirectEntryPrivateKeySource(readFileContent(private_key_path)),
        "", // passphrase
        description
    )

    SystemCredentialsProvider.getInstance().getStore().addCredentials(Domain.global(), c)
}

createGlobalMasterSSHCreds("jenkins", "/var/jenkins_home/.ssh/id_rsa", "Credentials for connecting slaves")

// eof
